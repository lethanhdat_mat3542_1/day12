<!DOCTYPE html>
<html lang="en">
<?php
$mysqli = new mysqli("localhost", "root", "", "qlsv");
session_start();
if (isset($_SESSION['gentle']) && isset($_SESSION['khoa']) && isset($_SESSION['hovaten']) && isset($_SESSION['born']) && isset($_SESSION['imgurl'])) {
    $diachi = null;
    if (!empty($_SESSION['diachi'])) {
        $diachi = $_SESSION['diachi'];
    }
    $explode = explode("/", $_SESSION['born']);
    $bornDate = date('Y-m-d H:i:s', strtotime(strval($explode[2]) . "-" . strval($explode[1]) . "-" . strval($explode[0]) . " 00:00:00"));
    $mysqli->query('INSERT INTO student(name,gender,faculty, birthday,address, avartar) VALUES ("' . $_SESSION['hovaten'] . '", "' . intval($_SESSION['gentle']) . '", "' . $_SESSION['khoa'] . '", "' . $bornDate . '", "' . $diachi . '", "' . $_SESSION['imgurl'] . '" )');
} else {
}
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Programming Day02</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js">
    </script>
    <style>
        .bold {
            font-weight: bold;
        }

        .danger {
            color: red;
        }

        body {
            display: flex;
            justify-content: center;
            margin-top: 40px;
        }

        .wrapper {
            border: 1.5px solid #4475a2;
            padding: 30px 60px;
        }

        label {
            background-color: rgb(85, 159, 39);
            width: 150px;
            display: inline-block;
            line-height: 30px;
            padding-left: 10px;
            color: white;
            border: 1.5px solid #4475a2;
        }

        .input-box {
            margin-bottom: 10px;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .input-radio {
            margin-bottom: 10px;
        }

        .button-box {
            margin-top: 30px;
            display: flex;
            justify-content: center;
        }

        .input-username {
            border: 1px solid #4475a2;
            margin-left: 30px;
            width: 300px;
        }

        .select-box {
            margin-left: 30px;

        }

        .button {
            background-color: rgb(85, 159, 39);
            ;
            color: white;
            padding: 12px 38px;
            border: 1.5px solid #4475a2;
            border-radius: 7px;
        }

        .khoa-classname {
            margin-left: 30px;
        }

        .khoa-select {
            display: flex;
            justify-content: start;
        }

        .txt {
            width: 200px;
            margin-left: 40px;
        }
    </style>
</head>

<body>
    <div class="wrapper text-center">
        <span>Bạn đã đăng ký thành công sinh viên</span> </br>
        <a href="form.php">Quay lại danh sách sinh viên</a>
    </div>

</body>

</html>